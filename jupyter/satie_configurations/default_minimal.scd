// exécuter ceci en premier
(
~satie = Satie.new(SatieConfiguration()) ;
~satie.boot() ;
)

// puis ceci
(
~satie.config.server.meter ;
~satie.config.server.plotTree ;
~satie.config.server.makeGui ;
)
