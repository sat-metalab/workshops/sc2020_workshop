#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created between Feb 15-27 2021

@author: Marc Lavallée <mlavallee@sat.qc.ca>


THX Logo Theme approximation, based on:
    https://musicinformationretrieval.com/thx_logo_theme.html

It implements a make_thx_voices function.
The transitional and the final notes are randomly generated,
so each execution of the function will create a slightly different version.

The main function will run the function and display graphs.

"""

import numpy as np
import matplotlib.pyplot as plt
from random import random
import librosa
import librosa.display

def detune(note_freq, cents):
    return note_freq * 2**(cents/1200)

def rand_cent(cents):
    min_cent, max_cent = cents[:2]
    return min_cent + random() * (max_cent - min_cent)

def make_thx_voices(A4=448, cents=[2, 5], plot=False):
    """
    Approximation of the THX logo theme, based on:
        https://musicinformationretrieval.com/thx_logo_theme.html

    Return an array of frequency transitions for 42 voices converging to 14 final notes
    """
    n_start = 0
    n_transition = 4
    n_sustain = 10
    n_decay = 13
    n_end = 15

    # Define time axis in units of seconds:
    t = np.arange(n_end)

    # ## Amplitude
    # For each of the four stages above, define amplitude envelope, $A(t)$.
    # The `start` stage, `sustain` stage, and `end` will have constant amplitudes:
    amplitude_start = 0.1
    amplitude_sustain = 1
    # amplitude_end = 0.005
    amplitude_end = 1

    # The `transition` and `decay` stages will increase or decrease
    # following a geometric/exponential sequence:
    amplitude_transition = np.geomspace(
        amplitude_start, amplitude_sustain, n_sustain - n_transition
    )
    amplitude_decay = np.geomspace(
        amplitude_sustain, amplitude_end, n_end - n_decay)

    # Create the amplitude envelope:
    amplitude = np.zeros(n_end)
    amplitude[n_start:n_transition] = amplitude_start
    amplitude[n_transition:n_sustain] = amplitude_transition
    amplitude[n_sustain:n_decay] = amplitude_sustain
    amplitude[n_decay:n_end] = amplitude_decay

    sustain_ratios = [1/12, 1/8, 1/6, 1/4, 1/3, 1/2, 2/3, 1, 4/3, 2, 8/3, 4, 16/3, 8]

    # build slightly detuned sustain notes (42 notes converging to 14 notes)
    f_sustain = [ detune(A4 * x, -rand_cent(cents)) for x in sustain_ratios ] \
        + [ A4 * x for x in sustain_ratios ] \
        + [ detune(A4 * x, rand_cent(cents)) for x in sustain_ratios ]

    # Total number of voices:
    num_voices = len(f_sustain)

    # Then, we create our time-dependent frequecies for each voice.
    fmin = librosa.note_to_hz("C0")
    fmax = librosa.note_to_hz("C9")

    voices = [None] * num_voices
    f_lo = librosa.note_to_hz("C3")

    for n_voice in range(num_voices):

        # Initialize frequency array.
        f = np.zeros(n_end)

        # Initial frequencies are random between C3 and C5.
        f1 = f_lo * 4 ** np.random.rand()
        f2 = f_lo * 4 ** np.random.rand()
        f_transition = f_lo * 4 ** np.random.rand()

        # Define one random time point during the start stage.
        n1 = np.random.randint(n_start, n_transition)

        # Assign frequencies to each stage.
        f[n_start:n1] = np.geomspace(f1, f2, n1 - n_start)
        f[n1:n_transition] = np.geomspace(f2, f_transition, n_transition - n1)
        f[n_transition:n_sustain] = np.geomspace(
            f_transition, f_sustain[n_voice], n_sustain - n_transition
        )

        f[n_sustain:n_end] = f_sustain[n_voice]

        # collect frequencies
        voices[n_voice] = f

    if plot:
        plt.style.use('seaborn-colorblind')
        plt.rcParams["axes.grid"] = True

        # Plot amplitude envelope with a log-y axis:
        plt.figure(figsize=(11, 4))
        plt.semilogy(t, amplitude, linewidth=2)
        plt.xlabel("Time (seconds)")
        plt.ylabel("Amplitude Envelope")

        # Plot frequency.
        plt.figure(figsize=(n_end, 9))
        for voice in voices:
            plt.semilogy(t, voice)
        plt.xlabel("Time (seconds)")
        plt.ylabel("Frequency (Hertz)")
        plt.ylim(fmin, fmax)
        plt.tick_params(labeltop=True, labelright=True)

        plt.show(block=False)

    return np.array(voices), amplitude


def main():
    return make_thx_voices(plot=True)

if __name__ == "__main__":
    voices, amplitude = main()
